<!doctype html>
<html lang="fr">
	<head>
  	<meta charset="utf-8">
	<title>Un Café en Attente - La plate-forme francophone du café en attente</title>
	<meta name="description" content="La plate-forme francophone du café en attente. Trouvez et marquez/déposez vos cafés en attente en en France, Belgique, etc.">
	<meta name="keywords" content="café en attente, suspended coffee, sandwich en attente, particpants café en attente, ville café en attente, annuaire café en attente, matthieu requenna, matthieurequenna.fr">
	<meta name="author" content="Matthieu Requenna">
	<meta name="robots" content="index,follow">
	<link href="https://plus.google.com/115030113496377511012/" rel="publisher" />
	<link rel="shortcut icon" href="templates/_cafeenattente/favicon.ico" type="image/x-icon">
	<link rel="icon" href="_site/templates/_cafeenattente/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="style.css" />
	</head>

	<body>
		<div id="container">
			<div id="barre-header">
				
			</div>
				<div id="logo-container">
				</div>
					<div id="logo-image">
						<img src="logo_couleur.png" />
					</div>
			
			<div id="page-contenu">	
				<div id="recherche">
					<input type="text" id="tape-recherche" value="Site en construction... à bientôt !"/>
				</div>
			</div>
		</div>
	</body>
</html>	