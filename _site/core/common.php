<?php
	if(isset($_GET))
	{
		foreach($_GET as $k=>$v)
		{
			if(!is_array($v))
			{
				$get[$k] = addslashes($classValidations->sanitize($k, $v));
			} else {
				foreach($v as $Ik=>$Iv)
				{
					$get[$k][$Ik] = addslashes($classValidations->sanitize($Ik, $Iv));
				}
			}
		}
	}

	if(isset($get['x'])) { unset($get['x']); }
	if(isset($get['y'])) { unset($get['y']); }

	if(isset($_POST))
	{
		foreach($_POST as $k=>$v)
		{
			if(!is_array($v))
			{
				$post[$k] = addslashes($classValidations->sanitize($k, $v));
			} else {
				$post[$k] = addslashes($v);
			}
		}
	}
	
	if(isset($post['x'])) { unset($post['x']); }
	if(isset($post['y'])) { unset($post['y']); }

?>