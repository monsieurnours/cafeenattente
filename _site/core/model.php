<?php
class Model
{
	public $table;
	protected $insertId;
	protected $table_fields;

	public function __construct(){
		$this->table_fields = array();
	}

	/**
	 *
	 * @read a record from a table
	 *
	 * @access public
	 *
	 * @param array $fields fields wanted
	 *
	 *
	 */
	public function read($req)
	{
		if(is_array($req))
		{
			$sql = "SELECT ";
			
			if(isset($req['select']))
			{
				$sql.= $req['select']['champs'];	
				$sql.= " FROM ".$req['select']['table'];	
			} else {
				$sql.= "*";
				$sql.= " FROM ".$this->table;
			}
	
			if(isset($req['innerjoin']))
			{
				foreach($req['innerjoin'] as $i)
				{
					$sql.= " INNER JOIN ".$i['table']." ON ".$i['condition'];	
				}
			}
			if(isset($req['where']))
			{
				if(!isset($req['where']['operateur'])) { $req['where']['operateur'] = '='; }
				$sql.= " WHERE ".$req['where']['champ']." ".$req['where']['operateur']." '".$req['where']['value']."'";	
			}
			if(isset($req['and']))
			{
				foreach($req['and'] as $a)
				{
					if(!isset($a['operateur'])) { $a['operateur'] = '='; }
					$sql.= " AND ".$a['champ']." ".$a['operateur']." '".$a['value']."'";	
				}
			}
			if(isset($req['or']))
			{
				foreach($req['or'] as $o)
				{
					if(!isset($o['operateur'])) { $o['operateur'] = '='; }
					$sql.= " OR ".$o['champ']." ".$o['operateur']." '".$o['value']."'";	
				}
			}
		} else {
			$sql = "SELECT * FROM ".$this->table." WHERE id = '$req'";
		}
		
		$result = mysql_query($sql);
		$data = mysql_fetch_assoc($result);
		
		if(!empty($data))
		{
			return $data;
		} else {
			echo 'Erreur ligne : '.__LINE__.' - '.mysql_error().'<br />';
			print_r($sql).'<br />';
		}
	}


	/*
	*
	*	Lecture par un champ précis
	*
	*/
	public function readBy($champ, $value)
	{
		$req = array(
			'where' => array(
				'champ' => $champ,
				'value' => $value
			)
		);	
		
		$datas = $this->read($req);
		return $datas;
	}

	/*
	*
	*	Find par un champ précis
	*
	*/
	public function findBy($champ, $value)
	{
		$req = array(
			'where' => array(
				'champ' => $champ,
				'value' => $value
			)
		);	
		
		$datas = $this->find($req);
		return $datas;
	}

	
	/**
	 *
	 * @find all records from a table
	 *
	 * @access public
	 *
	 * @param array $data conditions, fields and order / limit options
	 *
	 *
	 */
	public function find($req = array())
	{
		$sql = "SELECT ";
		
		if(isset($req['select']))
		{
			$sql.= $req['select']['champs'];
			if(isset($req['select']['table']))
			{
				$sql.= " FROM ".$req['select']['table'];	
			} else {
				$sql.= " FROM ".$this->table;
			}
		} else {
			$sql.= "*";
			$sql.= " FROM ".$this->table;
		}

		if(isset($req['innerjoin']))
		{
			foreach($req['innerjoin'] as $i)
			{
				$sql.= " INNER JOIN ".$i['table']." ON ".$i['condition'];	
			}
		}

		if(isset($req['leftjoin']))
		{
			foreach($req['leftjoin'] as $j)
			{
				$sql.= " LEFT JOIN ".$j['table']." ON ".$j['condition'];	
			}
		}
		
		if(isset($req['where']))
		{
			if(!isset($req['where']['operateur'])) { $req['where']['operateur'] = '='; }
			$sql.= " WHERE ".$req['where']['champ']." ".$req['where']['operateur']." '".$req['where']['value']."'";	
		}
		if(isset($req['and']))
		{
			foreach($req['and'] as $a)
			{
				if(!isset($a['operateur'])) { $a['operateur'] = '='; }
				$sql.= " AND ".$a['champ']." ".$a['operateur']." '".$a['value']."'";	
			}
		}
		if(isset($req['or']))
		{
			foreach($req['or'] as $o)
			{
				if(!isset($o['operateur'])) { $o['operateur'] = '='; }
				$sql.= " OR ".$o['champ']." ".$o['operateur']." '".$o['value']."'";	
			}
		}

		if(isset($req['and_or']))
		{
			if(isset($req['and_or']['and']))
			{
				if(!isset($req['and_or']['and']['operateur'])) { $req['and_or']['and']['operateur'] = '='; }
				$sql.= " AND (".$req['and_or']['and']['champ']." ".$req['and_or']['and']['operateur']." '".$req['and_or']['and']['value']."'";	
			}

			if(isset($req['and_or']['or']))
			{
				foreach($req['and_or']['or'] as $o)
				{
					if(!isset($o['operateur'])) { $o['operateur'] = '='; }
					$sql.= " OR ".$o['champ']." ".$o['operateur']." '".$o['value']."'";	
				}
			}
			
			$sql.= ') ';
		}

		if(isset($req['orderby']))
		{
			if(!isset($req['orderby']['asc']))
			{
				$sql.= " ORDER BY ".$req['orderby']['champ'];	
			} else {
				$sql.= " ORDER BY ".$req['orderby']['champ']." ".$req['orderby']['asc'];	
			}
		}
		if(isset($req['limit']))
		{
			$sql.= " LIMIT ".$req['limit']['start'].", ".$req['limit']['nb'];	
		}

		if(isset($req['requete']))
		{
			$data['requete'] = $sql;
		}
		
		$result = mysql_query($sql);

		if(!$result)
		{
			echo 'Erreur ligne : '.__LINE__.' - '.mysql_error().'<br />';			
			print_r($sql).'<br />';
		}

		if(isset($req['count']))
		{
			if(!empty($result))
			{
				$nb = mysql_num_rows($result);
				return $nb;
			} else {
				return 0;	
			}
		} else {
			if(!empty($result))
			{
				while($d = mysql_fetch_assoc($result)){
					if(isset($req['onlyIndex']))
					{
						$data[] = $d[$req['onlyIndex']];
					} elseif(isset($req['setIndex']))
					{
						$data[$d[$req['setIndex']['champ']]] = $d;
					} else {
						$data[] = $d;
					}
				}
			} else {
				$data = array();	
			}

			if(!empty($data)){
				return $data;
			} else {
				return false;
			}
		}
	}


	/*
	*
	*	Find simplifié avec un order
	*
	*/
	public function findOrder($champ = 'nom', $asc = 'ASC')
	{
		$req = array(
			'orderby' => array(
				'champ' => $champ,
				'asc' => $asc
			)
		);	
		
		$datas = $this->find($req);
		return $datas;
	}

	/**
	 *
	 * @find DISTINCT records from a table
	 *
	 * @access public
	 *
	 * @param array $data conditions, fields and order / limit options
	 *
	 *
	 */
	public function findDistinct($req = array())
	{
		$sql = "SELECT DISTINCT ";
		
		if(isset($req['select']))
		{
			$sql.= $req['select']['champs'];
			if(isset($req['select']['table']))
			{
				$sql.= " FROM ".$req['select']['table'];	
			} else {
				$sql.= " FROM ".$this->table;
			}
		}


		if(isset($req['orderby']))
		{
			if(!isset($req['orderby']['asc']))
			{
				$sql.= " ORDER BY ".$req['orderby']['champ'];	
			} else {
				$sql.= " ORDER BY ".$req['orderby']['champ']." ".$req['orderby']['asc'];	
			}
		}
		
		$result = mysql_query($sql);

		if(!$result)
		{
			echo 'Erreur ligne : '.__LINE__.' - '.mysql_error().'<br />';			
			print_r($sql).'<br />';
		}

		if(!empty($result))
		{
			while($d = mysql_fetch_assoc($result)){
				if(isset($req['onlyIndex']))
				{
					$data[] = $d[$req['onlyIndex']];
				} else {
					$data[] = $d;
				}
			}
		} else {
			$data = array();	
		}

		if(!empty($data)){
			return $data;
		} else {
			return false;
		}
	}

	/*
	*
	* Ajouter / editer process
	*
	*/			
	public function save($data = array(), $saveOnline = false) 
	{
		$this->getTableFields();
		$data = $this->unsetData($data);

		/*  si ID  alors UPDATE sinon INSERT */
		if(isset($data['ID']) && !empty($data['ID']))
		{
			$ID = $data['ID'];
			unset($data['ID']);
			
			$nb = count($data);
			$i = 1;
			
			$sql = "UPDATE ".$this->table." SET ";
			
			foreach($data as $key=>$value)
			{
				if($i < $nb)
				{
					$sql.= $key." = '".$value."', ";
				} else {
					$sql.= $key." = '".$value."'";
				}			
				$i++;
			}
			$sql.= " WHERE ID = '$ID'";
	
			$result = mysql_query($sql);

		} 
		else {
			$nb = count($data);
			$i = 1;
			$j = 1;
			
			$sql = "INSERT INTO ".$this->table." (";
				foreach($data as $key=>$value)
				{
					if($i < $nb)
					{
						$sql.= $key.", ";
					} else {
						$sql.= $key;
					}
					$i++;
				}
			$sql.= ") VALUES (";
				foreach($data as $key=>$value)
				{
					if($j < $nb)
					{
						$sql.= "'".$value."', ";
					} else {
						$sql.= "'".$value."'";
					}
					$j++;
				}
			$sql.= ")";
	
			$result = mysql_query($sql);

			$ID = mysql_insert_id();		
		}

		if($saveOnline == true)
		{
			$data['id'] = $id;
			$data['table'] = $this->table;
			$this->saveOnline($data);	
		}

		if(!$result)
		{
			echo 'Erreur ligne : '.__LINE__.' - '.mysql_error().'<br />';
		} else {
			return $ID;
		}
   	}	  

	/*
	*
	* Ajouter forcer => ajout malgré la présence du champ ID
	*
	*/			
	public function add_forced($data = array(), $saveOnline = false) 
	{
		$this->getTableFields();
		$data = $this->unsetData($data);

		$nb = count($data);
		$i = 1;
		$j = 1;
		
		$sql = "INSERT INTO ".$this->table." (";
			foreach($data as $key=>$value)
			{
				if($i < $nb)
				{
					$sql.= $key.", ";
				} else {
					$sql.= $key;
				}
				$i++;
			}
		$sql.= ") VALUES (";
			foreach($data as $key=>$value)
			{
				$value = trim($value);
				if($j < $nb)
				{
					$sql.= "'".$value."', ";
				} else {
					$sql.= "'".$value."'";
				}
				$j++;
			}
		$sql.= ")";

		$result = mysql_query($sql);

		$id = mysql_insert_id();		

		if(!$result)
		{
			echo 'Erreur ligne : '.__LINE__.' - '.mysql_error().'<br />';
		} else {
			return $id;
		}
   	}	  


	/* 
	*	tests distant :
	*		-> connexion
	*		-> authentification
	*		-> lancement script
	*/
	public function saveOnline($data)
	{
		$classConnexion = new Connexion();

		if($classConnexion->getConnexionState())
		{
			$data['auth_login'] = 'julien';
			$data['auth_pass'] 	= 'test';
			$data['auth_key'] 	= '132456';

/*			$data['auth']['key'] 	= $_SESSION['key'];
			$data['auth']['login'] 	= $_SESSION['login'];
			$data['auth']['pass'] 	= $_SESSION['pass'];
*/			$serializeData = serialize($data);
						
			$update = file_get_contents(_DISTANT_URL_.'remote/sql/save_datas.php?data='.$serializeData);
		}
	}
	

	/*
	*
	* Retourne un tableau contenant les noms des colonnes d'une table
	*
	*/			
	private function getTableFields()
	{
		$result = mysql_query("SELECT * FROM ".$this->table." ");

		$nbFields = mysql_num_fields($result);
		for($i=0; $i < $nbFields; $i++) 
		{
		   $var = mysql_field_name($result, $i);
		   $fields[] = $var;
		}
		$this->table_fields = $fields;
	}


	/*
	*
	* Retourne le tableau data nettoyé des champs inutiles
	*
	*/			
	private function unsetData($data)
	{
		foreach($data as $k=>$v)
		{
			if(!in_array($k, $this->table_fields))
			{
				unset($data[$k]);
			}
		}
		return $data;
	}


	/**
	*	modifier etat publication
	*/
	public function edit_publish($id, $state)
	{
		$sql = "UPDATE ".$this->table." 
					SET publish = '$state'
					WHERE id = '$id'";
		$result = mysql_query($sql);

		if(!$result)
		{
			echo 'Erreur ligne : '.__LINE__.' - '.mysql_error().'<br />';
		} else {
			return true;
		}
	}

	/**
	*	delete
	*/
	public function del($id)
	{
		$sql = "DELETE FROM ".$this->table." 
					WHERE id = '$id'";
		$result = mysql_query($sql);

		if(!$result)
		{
			echo 'Erreur ligne : '.__LINE__.' - '.mysql_error().'<br />';
		} else {
			return true;
		}
	}

	/**
	*	truncate table
	*/
	public function truncate()
	{
		$sql = "TRUNCATE TABLE ".$this->table."  ";
		$result = mysql_query($sql);

		if(!$result)
		{
			echo 'Erreur ligne : '.__LINE__.' - '.mysql_error().'<br />';
		} else {
			return true;
		}
	}


	/**
	 *
	 * @pour chaque resultats regard les champs préfixés par une langue
	 *		-> si champ avec langue courante retourne le champ ss le prefix avec la valeur
	 *		-> si champ avec prefix mais pas de langue courante, supprime le resultat
	 *
	 * @access private
	 *
	 * @param array $data
	 *
	 *
	 */
	private function sortResultsByLangs($d)
	{
		global $langue;
		$langues = array('fr', 'uk');

		foreach($d as $k=>$v)
		{
			$pref = substr($k, 0, 2);
			/* si dans le tableau langues */
			if(in_array($pref, $langues))
			{
				/* si prefix = langue courante */
				if($pref == $langue)
				{
					/*  regard si 3ème caractère est un underscore, alors supprimer les 3 premiers caract sinon les 2 premiers (cas d'une jointure) */
					if(substr($k, 2, 1) == '_')
					{
						/* retourne champ sans prefix fr_  */
						$kn = substr($k, 3, strlen($k));
					} else {
						/* retourne champ sans prefix fr  */
						$kn = substr($k, 2, strlen($k));
					}
					$d[$kn] = $v;
					unset($d[$k]);
				} else {
					/*  si champ avec prefix mais pas de langue courante -> suppr */
					unset($d[$k]);
				}
			}
		}
		return $d;	
	}


	
	/**
	 *
	 *	pagination
	 *
	 */
	public function paginate($data)
	{
		extract($data);
		
		$page_total 	= ceil($count_publish / $item_page);
		
		if($page_courante <= 1)
		{
			$d['start'] = 0;		
		} else {
			$d['start'] = ($page_courante - 1) * $item_page;		
		}

		for($i = 1; $i <= $page_total; $i++)
		{
			$d['pages'][$i] = $i;
		}
		
		return $d;
	}

}
?>