<?php
class Validations
{
	/*
	*
	*	Va permetrre de créer un tableau complet depuis un fichier txt en parcourant chaque ligne et en decoupant chaque ligne suivant le séprateur
	*
	*/
	public function getFile2Array($file)
	{
		$separateur = ";";
		$fp=fopen($file,'r');
		
		$getDatas='';
		while (!feof($fp))
		{
			$ligne = fgets($fp,4096);
			$liste = explode($separateur,$ligne);
			$taille = count($liste);
			
			switch($taille)
			{
				case 2:
					$getDatas[$liste[0]] = $liste[1];
				break;	
				case 3:
					$getDatas[$liste[0]][$liste[1]] = $liste[2];
				break;
				case 4:
					$getDatas[$liste[0]][$liste[1]][$liste[2]] = $liste[3];
				break;
				case 5:
					$getDatas[$liste[0]][$liste[1]][$liste[2]][$liste[3]] = $liste[4];
				break;
			}
		}
		fclose($fp);
		
		return $getDatas;
	}


	public function sanitize($key, $data)
	{
		$data = strip_tags($data);
		$data = trim($data);
		
		return $data;
	}
	
	public function truncStr($str, $length)
	{
		$str = strip_tags($str);
		if(strlen($str) > $length)
		{
			$str = substr($str, 0, $length);
			$last_space = strrpos($str, " ");
			$str = substr($str, 0, $last_space)."...";
		}
		
		return $str;		
	}

	public function truncStrSearch($str, $words, $length)
	{
		$str = strip_tags($str);
		$strPos = stripos($str, $words);
		if($strPos < 30)
		{
			$recStr = $strPos;	
		} else {
			$recStr = 30;
		}
		$str = substr($str, $strPos-$recStr);
		$first_space = stripos($str, " ");
		$str = substr($str, $first_space);

		if(strlen($str) > $length)
		{
			$str = substr($str, 0, $length);
			$last_space = strrpos($str, " ");
			$str = "...".substr($str, 0, $last_space)."...";
		}
		
		return $str;		
	}
	
	public function cleanHref($str)
	{
			//Suppression des espaces en début et fin de chaîne
			$str = trim($str);
			//Suppression des accents
			$trans = array(
				"À" => "a", "Á" => "a", "Â" => "a", "Ã" => "a", "Ä" => "a", "Å" => "a", "à" => "a", "á" => "a", "â" => "a", "ã" => "a", "ä" => "a", "å" => "a", "Ò" => "o", "Ó" => "o", "Ô" => "o", 
				"Õ" => "o", "Ö" => "o", "Ø" => "o", "ò" => "o", "ó" => "o", "ô" => "o", "õ" => "o", "ö" => "o", "ø" => "o", 
				"È" => "e", "É" => "e", "Ê" => "e", "Ë" => "e", "é" => "e", "è" => "e", "ê" => "e", "ë" => "e", 
				"Ç" => "c", "ç" => "c", 
				"Ì" => "i", "Í" => "i", "Î" => "i", "Ï" => "i", "ì" => "i", "í" => "i", "î" => "i", "ï" => "i", 
				"Ù" => "u", "Ú" => "u", "Û" => "u", "Ü" => "u", "ù" => "u", "ú" => "u", "û" => "u", "ü" => "u", 
				"ÿ" => "y", "Ñ" => "n", "ñ" => "n"
			);
			$str = strtr($str, $trans);
			//mise en minuscule
			$str = strtolower($str);
			//Suppression des espaces et caracteres spéciaux
			$str = str_replace(" ",'_',$str);
			$str = str_replace(".",'_',$str);
			$str = str_replace("-",'_',$str);
			$str = preg_replace('#([^a-z0-9-.])#','_',$str);
			// Suppression des mots 'le' 'la' 'du'
			$str = str_replace("_le_",'_',$str);
			$str = str_replace("_la_",'_',$str);
			$str = str_replace("_l_",'_',$str);
			$str = str_replace("_les_",'_',$str);
			$str = str_replace("_du_",'_',$str);
			$str = str_replace("_des_",'_',$str);
			$str = str_replace("_sur_",'_',$str);
			$str = str_replace("_a_",'_',$str);
			$str = str_replace("_au_",'_',$str);
			$str = str_replace("_aux_",'_',$str);
			$str = str_replace("_un_",'_',$str);
			$str = str_replace("_une_",'_',$str);
			//Suppression des tirets multiples
			$str = preg_replace('#([_]+)#','_',$str);
			//Suppression du premier caractère si c'est un tiret
			if($str{0} == '_') { $str = substr($str,1); }
			//Suppression du dernier caractère si c'est un tiret
			if(substr($str, -1, 1) == '_') { $str = substr($str, 0, -1); }
		
		return $str;
	}

	public function convertDateFr2Uk($date)
	{
		$date = str_replace('/', '', $date);
		$date = str_replace('-', '', $date);
		$date = str_replace(' ', '', $date);
		$j = substr($date, 0, 2);
		$m = substr($date, 2, 2);
		$a = substr($date, 4, 8);
		$n_date = $a.'-'.$m.'-'.$j;
		return $n_date;
	}

	public function cleanDateUK($date)
	{
		$date = str_replace('/', '', $date);
		$date = str_replace('-', '', $date);
		$date = str_replace(' ', '', $date);
		$a = substr($date, 0, 4);
		$m = substr($date, 4, 2);
		$j = substr($date, 6, 2);
		$n_date = $a.'-'.$m.'-'.$j;
		return $n_date;
	}

	public function convertDateUk2Fr($date)
	{
		$date = str_replace('/', '', $date);
		$date = str_replace('-', '', $date);
		$date = str_replace(' ', '', $date);
		$a = substr($date, 0, 4);
		$m = substr($date, 4, 2);
		$j = substr($date, 6, 2);
		$n_date = $j.'/'.$m.'/'.$a;
		return $n_date;
	}

	public function cleanHeure($heure)
	{
		$heure = str_replace(':', '', $heure);
		$h = substr($heure, 0, 2);
		$m = substr($heure, 2, 2);
		$s = substr($heure, 4, 2);
		$n_heure = $h.'h'.$m;
		return $n_heure;
	}

	public function splitTemps($heure)
	{
		$h = substr($heure, 0, 2);
		$m = substr($heure, 2, 2);
		return $h.':'.$m;
	}

	public function cleanTemps($heure)
	{
		$heure = str_replace(':', '', $heure);
		$h = substr($heure, 0, 2);
		$m = substr($heure, 2, 2);
		$s = substr($heure, 4, 2);
		$n_temps = $m.':'.$s;
		return $n_temps;
	}

	public function calculTemps($debut, $fin)
	{
		$fin = str_replace(':', '', $fin);
		$h_f = substr($fin, 0, 2);
		$m_f = substr($fin, 2, 2);
		$s_f = substr($fin, 4, 2);

		$fin = mktime($h_f, $m_f, $s_f);

		if($debut != "00:00:00")
		{
			$debut = str_replace(':', '', $debut);
			$h_d = substr($debut, 0, 2);
			$m_d = substr($debut, 2, 2);
			$s_d = substr($debut, 4, 2);
	
			$debut = mktime($h_d, $m_d, $s_d);
			$n_temps = $fin - $debut;	
		} else {
			$n_temps = $fin;
		}

/*
		$h = $h_f - $h_d;
		$m = $m_f - $m_d;
		$s = $s_f - $s_d;

		$n_temps = $h.':'.$m.':'.$s;
*/		return $n_temps;
	}

	public function convertHeure2UK($heure)
	{
		$heure = str_replace('h', '', $heure);
		$h = substr($heure, 0, 2);
		$m = substr($heure, 2, 2);
		$s = substr($heure, 4, 2);
		$n_heure = $h.':'.$m;
		return $n_heure;
	}

	/*
	*
	* Validation d'un champ Email
	*
	*/	
	public function checkValidEmail($email) {
	  // First, we check that there's one @ symbol, and that the lengths are right.
	  if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
		// Email invalid because wrong number of characters in one section or wrong number of @ symbols.
		return false;
	  }
	  
	  // Split it into sections to make life easier
	  $email_array = explode("@", $email);
	  $local_array = explode(".", $email_array[0]);
	  for ($i = 0; $i < sizeof($local_array); $i++) {
		if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&↪'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
		  return false;
		}
	  }
	  
	  // Check if domain is IP. If not, it should be valid domain name
	  if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) {
		$domain_array = explode(".", $email_array[1]);
		if (sizeof($domain_array) < 2) {
			return false;
		}
		for ($i = 0; $i < sizeof($domain_array); $i++) {
		  if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])| ↪([A-Za-z0-9]+))$", $domain_array[$i])) {
			return false;
		  }
		}
	  }
	  return true;
	}

}
?>