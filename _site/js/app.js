function active_recherche(){
	var panneau_enc = $('#recherche_en').val();
	if (panneau_enc == '0')
	{
		$('#panneau_enc').val('1');
		$('#recherche').css('top', '60px');
		$('#recherche').css('right', '10px');
		$('#recherche').css('margin-right', '0px');
		$('#recherche').css('width', '300px');
		$('#recherche').css('height', '40px');
		$('#recherche').css('background', 'url(assets/icone/icon-recherchedfdf.png) no-repeat left center #FFFFFF');
		$('#tape-recherche').css('font-size', '16px');
		$('#tape-recherche').css('padding-top', '6px');
		$('#tape-recherche').css('padding-bottom', '6px');
		$('#tape-recherche').css('margin-left', '5px');
		$('#tape-recherche').css('margin-right', '5px');
		$('#tape-recherche').css('width', '96%');
		setTimeout(function() {$("#list_resultat").fadeToggle("slow");},1500);
	}	
}

function paralaxe(decalage){
	var decalage_px = "-" + decalage + "px";
	$('#image-fond-image').css('margin-left', decalage_px);
}

function panneau(id)
{
	var panneau_enc = $('#panneau_vu').val();
	if(id != panneau_enc) {
		var panneau_prev = "#panneau_" + panneau_enc;
		var panneau = "#panneau_" + id;
		$(panneau_prev).css('margin-left', '-100%');
		$(panneau).css('display', 'block');
		$(panneau).css('margin-left', '0px');
		$('#panneau_vu').val(id);
		
		setTimeout(function() {$(panneau_prev).css('tansition', '0s');},1500);
		setTimeout(function() {$(panneau_prev).css('display', 'none');},1500);
		setTimeout(function() {$(panneau_prev).css('margin-left', '100%');},2000);
		setTimeout(function() {$(panneau_prev).css('display', 'block');},2500);
		setTimeout(function() {$(panneau_prev).css('tansition', '1s');},2500);
	}	
}


function inscription (){
	var nom = $('#nom').val();
	var prenom = $('#prenom').val();
	var pseudo = $('#pseudo').val();
	var mail = $('#mail').val();
	var pass = $('#pass').val();
	var data_post = "nom=" + nom + "&prenom="+ prenom + "&pseudo="+ pseudo + "&mail="+ mail + "&pass="+ pass;
	AAWrite2('ajax_inscription.php', data_post, 'div', 'inscription');
}

function login (){
	var mail = $('#mail_login').val();
	var pass = $('#pass_login').val();
	var data_post = "mail="+ mail + "&pass="+ pass;
	alert(data_post);
	AAWrite2('ajax_login.php', data_post, 'div', 'identification');
}

(function($){
   $(window).load(function(){
      $(".content").mCustomScrollbar();
   });
})(jQuery);

if (navigator.geolocation)
{
  navigator.geolocation.getCurrentPosition(function(position)
  {
    // alert("Latitude : " + position.coords.latitude + ", longitude : " + position.coords.longitude);
  });
}
else
{
  // alert("Votre navigateur ne prend pas en compte la géolocalisation HTML5");
} 
  

$(function() {
	var anchorName = document.location.hash.substring(1);
	if(anchorName != '')
	{
		panneau(anchorName);
	}	
});