<?php require_once('dir.inc.php'); ?>
<!doctype html>
<html lang="fr">
	<head>
  	<meta charset="utf-8">
	<meta name="description" content="La plate-forme française du café en attente. Trouvez et marquez/déposez vos cafés en attente en France métropolitaine.">
	<meta name="keywords" content="café en attente, suspended coffee, sandwich en attente, particpants café en attente, ville café en attente, annuaire café en attente, matthieu requenna, matthieurequenna.fr">
		<title>Un Café en Attente - La plate-forme française du café en attente</title>
		<?php include(''.$DIR.'templates/_'.$TEMPLATE.'/includes/header.inc.php'); ?>
		<div id="panneau_1">
			<div id="recherche">
				<input type="text" id="tape-recherche" value="Rechercher une ville, une région, un lieu..." onfocus="this.value=''" onkeypress="active_recherche()"/>
			</div>
			<div class="contenu_panneau" id="list_resultat">
				<div class="content">
					
				</div>
			</div>
		</div>

		<div id="panneau_2">
			<div class="contenu_panneau">
				<h1>ANNUAIRE DES CAFES EN ATTENTE&nbsp;</h1>
				<div class="content">
					<p>
						<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla porro reprehenderit est ut soluta aspernatur ab ipsa sint maxime cupiditate molestias laudantium alias quos quis eveniet adipisci harum unde nostrum!</span>
						<span>Soluta tenetur suscipit quaerat obcaecati optio ipsum possimus magnam assumenda repellat dolores aliquam non id! Explicabo harum eos quisquam quae architecto delectus iure necessitatibus nihil reprehenderit facere blanditiis sed dolor!</span>
						<span>Vitae reiciendis doloremque ullam culpa quas itaque animi perspiciatis distinctio alias atque quam quae laborum veritatis aliquid dicta voluptatibus dolore sequi aspernatur qui minus doloribus in deleniti assumenda repellendus eum!</span>
						<span>Error iusto optio minima eum veniam sapiente aliquid iste libero tempore reiciendis reprehenderit quae facere architecto illum dolor facilis modi qui harum in consequatur. A nobis reprehenderit eos modi reiciendis!</span>
						<span>Quaerat culpa facilis nisi ea recusandae eos commodi voluptate sint voluptatum consequatur natus nostrum sunt alias beatae dolore labore vitae enim omnis nam rem. Tempora similique inventore perferendis vero exercitationem!</span>
						<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla porro reprehenderit est ut soluta aspernatur ab ipsa sint maxime cupiditate molestias laudantium alias quos quis eveniet adipisci harum unde nostrum!</span>
						<span>Soluta tenetur suscipit quaerat obcaecati optio ipsum possimus magnam assumenda repellat dolores aliquam non id! Explicabo harum eos quisquam quae architecto delectus iure necessitatibus nihil reprehenderit facere blanditiis sed dolor!</span>
						<span>Vitae reiciendis doloremque ullam culpa quas itaque animi perspiciatis distinctio alias atque quam quae laborum veritatis aliquid dicta voluptatibus dolore sequi aspernatur qui minus doloribus in deleniti assumenda repellendus eum!</span>
						<span>Error iusto optio minima eum veniam sapiente aliquid iste libero tempore reiciendis reprehenderit quae facere architecto illum dolor facilis modi qui harum in consequatur. A nobis reprehenderit eos modi reiciendis!</span>
						<span>Quaerat culpa facilis nisi ea recusandae eos commodi voluptate sint voluptatum consequatur natus nostrum sunt alias beatae dolore labore vitae enim omnis nam rem. Tempora similique inventore perferendis vero exercitationem!</span>
						<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla porro reprehenderit est ut soluta aspernatur ab ipsa sint maxime cupiditate molestias laudantium alias quos quis eveniet adipisci harum unde nostrum!</span>
						<span>Soluta tenetur suscipit quaerat obcaecati optio ipsum possimus magnam assumenda repellat dolores aliquam non id! Explicabo harum eos quisquam quae architecto delectus iure necessitatibus nihil reprehenderit facere blanditiis sed dolor!</span>
						<span>Vitae reiciendis doloremque ullam culpa quas itaque animi perspiciatis distinctio alias atque quam quae laborum veritatis aliquid dicta voluptatibus dolore sequi aspernatur qui minus doloribus in deleniti assumenda repellendus eum!</span>
						<span>Error iusto optio minima eum veniam sapiente aliquid iste libero tempore reiciendis reprehenderit quae facere architecto illum dolor facilis modi qui harum in consequatur. A nobis reprehenderit eos modi reiciendis!</span>
						<span>Quaerat culpa facilis nisi ea recusandae eos commodi voluptate sint voluptatum consequatur natus nostrum sunt alias beatae dolore labore vitae enim omnis nam rem. Tempora similique inventore perferendis vero exercitationem!</span>
						<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla porro reprehenderit est ut soluta aspernatur ab ipsa sint maxime cupiditate molestias laudantium alias quos quis eveniet adipisci harum unde nostrum!</span>
						<span>Soluta tenetur suscipit quaerat obcaecati optio ipsum possimus magnam assumenda repellat dolores aliquam non id! Explicabo harum eos quisquam quae architecto delectus iure necessitatibus nihil reprehenderit facere blanditiis sed dolor!</span>
						<span>Vitae reiciendis doloremque ullam culpa quas itaque animi perspiciatis distinctio alias atque quam quae laborum veritatis aliquid dicta voluptatibus dolore sequi aspernatur qui minus doloribus in deleniti assumenda repellendus eum!</span>
						<span>Error iusto optio minima eum veniam sapiente aliquid iste libero tempore reiciendis reprehenderit quae facere architecto illum dolor facilis modi qui harum in consequatur. A nobis reprehenderit eos modi reiciendis!</span>
						<span>Quaerat culpa facilis nisi ea recusandae eos commodi voluptate sint voluptatum consequatur natus nostrum sunt alias beatae dolore labore vitae enim omnis nam rem. Tempora similique inventore perferendis vero exercitationem!</span>
						<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla porro reprehenderit est ut soluta aspernatur ab ipsa sint maxime cupiditate molestias laudantium alias quos quis eveniet adipisci harum unde nostrum!</span>
						<span>Soluta tenetur suscipit quaerat obcaecati optio ipsum possimus magnam assumenda repellat dolores aliquam non id! Explicabo harum eos quisquam quae architecto delectus iure necessitatibus nihil reprehenderit facere blanditiis sed dolor!</span>
						<span>Vitae reiciendis doloremque ullam culpa quas itaque animi perspiciatis distinctio alias atque quam quae laborum veritatis aliquid dicta voluptatibus dolore sequi aspernatur qui minus doloribus in deleniti assumenda repellendus eum!</span>
						<span>Error iusto optio minima eum veniam sapiente aliquid iste libero tempore reiciendis reprehenderit quae facere architecto illum dolor facilis modi qui harum in consequatur. A nobis reprehenderit eos modi reiciendis!</span>
						<span>Quaerat culpa facilis nisi ea recusandae eos commodi voluptate sint voluptatum consequatur natus nostrum sunt alias beatae dolore labore vitae enim omnis nam rem. Tempora similique inventore perferendis vero exercitationem!</span>
						<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla porro reprehenderit est ut soluta aspernatur ab ipsa sint maxime cupiditate molestias laudantium alias quos quis eveniet adipisci harum unde nostrum!</span>
						<span>Soluta tenetur suscipit quaerat obcaecati optio ipsum possimus magnam assumenda repellat dolores aliquam non id! Explicabo harum eos quisquam quae architecto delectus iure necessitatibus nihil reprehenderit facere blanditiis sed dolor!</span>
						<span>Vitae reiciendis doloremque ullam culpa quas itaque animi perspiciatis distinctio alias atque quam quae laborum veritatis aliquid dicta voluptatibus dolore sequi aspernatur qui minus doloribus in deleniti assumenda repellendus eum!</span>
						<span>Error iusto optio minima eum veniam sapiente aliquid iste libero tempore reiciendis reprehenderit quae facere architecto illum dolor facilis modi qui harum in consequatur. A nobis reprehenderit eos modi reiciendis!</span>
						<span>Quaerat culpa facilis nisi ea recusandae eos commodi voluptate sint voluptatum consequatur natus nostrum sunt alias beatae dolore labore vitae enim omnis nam rem. Tempora similique inventore perferendis vero exercitationem!</span>
						<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla porro reprehenderit est ut soluta aspernatur ab ipsa sint maxime cupiditate molestias laudantium alias quos quis eveniet adipisci harum unde nostrum!</span>
						<span>Soluta tenetur suscipit quaerat obcaecati optio ipsum possimus magnam assumenda repellat dolores aliquam non id! Explicabo harum eos quisquam quae architecto delectus iure necessitatibus nihil reprehenderit facere blanditiis sed dolor!</span>
						<span>Vitae reiciendis doloremque ullam culpa quas itaque animi perspiciatis distinctio alias atque quam quae laborum veritatis aliquid dicta voluptatibus dolore sequi aspernatur qui minus doloribus in deleniti assumenda repellendus eum!</span>
						<span>Error iusto optio minima eum veniam sapiente aliquid iste libero tempore reiciendis reprehenderit quae facere architecto illum dolor facilis modi qui harum in consequatur. A nobis reprehenderit eos modi reiciendis!</span>
						<span>Quaerat culpa facilis nisi ea recusandae eos commodi voluptate sint voluptatum consequatur natus nostrum sunt alias beatae dolore labore vitae enim omnis nam rem. Tempora similique inventore perferendis vero exercitationem!</span>
						<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla porro reprehenderit est ut soluta aspernatur ab ipsa sint maxime cupiditate molestias laudantium alias quos quis eveniet adipisci harum unde nostrum!</span>
						<span>Soluta tenetur suscipit quaerat obcaecati optio ipsum possimus magnam assumenda repellat dolores aliquam non id! Explicabo harum eos quisquam quae architecto delectus iure necessitatibus nihil reprehenderit facere blanditiis sed dolor!</span>
						<span>Vitae reiciendis doloremque ullam culpa quas itaque animi perspiciatis distinctio alias atque quam quae laborum veritatis aliquid dicta voluptatibus dolore sequi aspernatur qui minus doloribus in deleniti assumenda repellendus eum!</span>
						<span>Error iusto optio minima eum veniam sapiente aliquid iste libero tempore reiciendis reprehenderit quae facere architecto illum dolor facilis modi qui harum in consequatur. A nobis reprehenderit eos modi reiciendis!</span>
						<span>Quaerat culpa facilis nisi ea recusandae eos commodi voluptate sint voluptatum consequatur natus nostrum sunt alias beatae dolore labore vitae enim omnis nam rem. Tempora similique inventore perferendis vero exercitationem!</span>
					</p>
				</div>
			</div>
		</div>	
		
		<div id="panneau_3">
			<div class="contenu_panneau">
				<h1>LE CAFE EN ATTENTE, C'EST QUOI ?&nbsp;</h1>
				<div class="content">
				</div>
			</div>
		</div>	
		
		<div id="panneau_4">
			<div class="contenu_panneau">
				<h1>VOUS SOUHAITEZ PARTICIPER ?&nbsp;</h1>
				<div class="content">
				</div>
			</div>
		</div>
		<?php if (isset($user_id) && $user_id != '0' && $user_id !='')
		{
		?>
		<div id="panneau_5">
			<div class="contenu_panneau" id="mon_compte">
				<h1>GERER VOTRE COMPTE&nbsp;</h1>
				<div class="content">
				</div>
			</div>
		</div>
		<?php	
		}
		else
		{
		?>	
			<div id="panneau_5">
				<div class="contenu_panneau" id="mon_compte">
					<h1>VOUS IDENTIFIER&nbsp;</h1>
					<div class="content">
						<div id="identification" class="grid half xright">
							<h2 class="xright">J'ai déjà un compte</h2>
							<p class="xright">Veuillez renseigner vos identifiants</p>
							<div class="gras xleft">Adresse e-mail :</div>
							<input id="mail_login" name="mail" class="champ" type="text"/>
							<div class="gras xleft">Mot de passe :</div>
							<input id="pass_login" name="pass" class="champ" type="password"/>
							<input type="button" class="validate" value="S'IDENTIFIER" onclick="login()"/>
						</div>
						<div id="inscription" class="grid half xright">
							<h2 class="xright">Je souhaite m'inscrire</h2>
							<p class="xright">Echangez avec la communauté cafeenattente.org</p>
							<div class="gras xleft">Pseudonyme :</div>
							<input id="pseudo" name="pseudo" class="champ" type="text"/>
							<div class="gras xleft">Nom :</div>
							<input id="nom" name="nom" class="champ" type="text"/>
							<div class="gras xleft">Prénom :</div>
							<input id="prenom" name="prenom" class="champ" type="text"/>
							<div class="gras xleft">Adresse e-mail :</div>
							<input id="mail" name="mail" class="champ" type="text"/>
							<div class="gras xleft">Mot de passe :</div>
							<input id="pass" name="pass" class="champ" type="password"/>
							<input type="button" class="validate" value="S'INSCRIRE" onclick="inscription()"/>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
		?>	
		<input id="panneau_vu" type="hidden" value="1" />
		<input id="recherche_en" type="hidden" value="0" />
		<?php include(''.$DIR.'templates/_'.$TEMPLATE.'/includes/footer.inc.php'); ?>