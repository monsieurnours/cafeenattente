		<meta name="author" content="Matthieu Requenna">
		<meta name="robots" content="index,follow">
		<link href="https://plus.google.com/115030113496377511012/" rel="publisher" />
		<link rel="shortcut icon" href="<?php echo(''.$DIR.'templates/_'.$TEMPLATE.'/favicon.ico'); ?>" type="image/x-icon">
		<link rel="icon" href="<?php echo(''.$DIR.'templates/_'.$TEMPLATE.'/favicon.ico'); ?>" type="image/x-icon">
		<link rel="stylesheet" href="<?php echo(''.$DIR.'templates/_'.$TEMPLATE.'/style.css'); ?>" />
		<link rel="stylesheet" href="<?php echo(''.$DIR.'templates/_'.$TEMPLATE.'/jquery.mCustomScrollbar.css'); ?>" />
		<script type='text/javascript' src='<?php echo $DIR; ?>js/jwplayer/swfobject.js'></script>
		<script type="text/javascript" src="<?php echo $DIR; ?>js/anaa.js"></script>
		<script type="text/javascript" src="<?php echo $DIR; ?>js/jquery.js"></script>
		<script type="text/javascript" src="<?php echo $DIR; ?>js/app.js"></script>
		<script src="<?php echo $DIR; ?>js/jquery.mCustomScrollbar.js"></script>
	</head>

	<body>
		<div id="container">
			<div id="barre-header">
				<nav id="navigation-header">
					<ul>
						<li onclick="paralaxe(0); panneau(1);"><a href="#1">ACCUEIL</a></li>
						<li onclick="paralaxe(200); panneau(2);"><a href="#2">ANNUAIRE</a></li>
						<li onclick="paralaxe(400); panneau(3);"><a href="#3">LE CONCEPT</a></li>
						<li onclick="paralaxe(600); panneau(4);"><a href="#4">PARTICIPER</a></li>
						<?php if (isset($user) && $user != '0' && $user !='')
						{
						?>
							<li id="espace_login" onclick="paralaxe(400); panneau(5);"><a href="#5">VOTRE COMPTE</a></li>
						<?php	
						}
						else
						{
						?>	
							<li id="espace_login" onclick="paralaxe(400); panneau(5);"><a href="#5">S'IDENTIFIER</a></li>
						<?php
						}
						?>	
					</ul>	
				</nav>
			</div>
				<div id="logo-container">
				</div>
					<div id="logo-image">
						<img src="<?php echo(''.$DIR.'templates/_'.$TEMPLATE.'/assets/design/logo_couleur.png'); ?>" />
					</div>
			<div id="image-fond">
				<div id="image-fond-image">
				</div>
			</div>
			<div id="page-contenu">         